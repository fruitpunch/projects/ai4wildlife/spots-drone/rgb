<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/Samuel-Sendzul/AI-For-Wildlife-RGB">
    <img src="assets/FruitPunch_AI_Logo_Light_4kpx.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">RGB - AI for Wildlife</h3>

  <p align="center">
    A model testing environment for finetuning and validating various pretrained object detection algorithms for detecting humans in drone footage.
    <br />
    <a href="https://github.com/Samuel-Sendzul/AI-For-Wildlife-RGB"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://github.com/github_username/repo_name">View Demo</a>
    ·
    <a href="https://github.com/Samuel-Sendzul/AI-For-Wildlife-RGB/issues">Report Bug</a>
    ·
    <a href="https://github.com/Samuel-Sendzul/AI-For-Wildlife-RGB/issues">Request Feature</a>
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project
A model testing environment for finetuning and validating various pretrained object detection algorithms for detecting humans in drone footage.

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple example steps.

### Prerequisites

Set up a Python virtual environment with pip installed and install the requirements.txt file:
```
pip install -r requirements.txt
```

Download the SPOTS Annotated Images dataset off of Google Drive and place it in your projects root directory.
Download the Retinanet model checkpoint trained on the SPOTS data, the FasterRCNN model checkpoint, and the EfficientNet model checkpoint from Google Drive and place it in models/cache (you will need to create these folders).


<!-- USAGE EXAMPLES -->
## Usage

### Training

#### Object Detector
To train an object detection model, run the ```training.py``` script with the applicable command line arguments. You will also need to point the script to the correct file paths for the training and validation set image folders and annotation files. (See the ```training.py```  script for an example). A model checkpoint is saved after every epoch of training.

The model was trained on 400x400 subframes obtained from the 3840x2160 raw input frames. You will get best results if you cut your raw input frames into subframes (see ```video_inference.py``` for an example).

You will also need to replace the compile_{model}() (where {model} is replaced with the name of your model (see Swapping Out Models)) method with the applicable model compilation method to ensure you are training the correct model. For example, to train the RetinaNet implementation, you will need to use compile_retinanet() to compile the RetinaNet model.

For example, an execution of the training script would look as follows:

```
train.py --batch_size 8 --num_epochs 10 --model_name retinanet
```

#### Image Classifier
To train the image classifier, navigate to the efficientnet_classifier folder and run the ```training.py```  script with the applicable command line arguments. A single model checkpoint is saved for future use.

The model was trained on 400x400 subframes obtained from the 3840x2160 raw input frames that were then resized down to 224x224 (EfficientNetB0's input size). Best results will be obtained by predicting on 400x400 subframes obtained from the original, 3840x2160 raw images.

#### Swapping Out Object Detection Models
To test different object detection model architectures, a method within the ObjectDetector class (```object_detector.py```) that compiles the complete PyTorch object detection
model needs to writtin (see compile_retinanet() in object_detector.py as an example).

To use different datasets to train your model, you need to write a class that inherets from torch.utils.data.Dataset that contains a __getitem__ and a __len__ method 
(see data/spots_dataset.py) as an example. Then, create your dataset object in the train.py script. 

### Inference

#### Object Detector 
In the ObjectDetector class (found in ```object_detector.py``` ), you will find a predict method. You can use this method to infer on batches of images. You can then use the predictions returned from this method to annotate images. Non-max suppression is included in the predict method.

You can see an example of object detection inference in ```object_detection_inference_example.py```. Both FasterRCNN and RetinaNet are implemented there for testing and comparing results.


### Image Classifier
The EfficientNetPoacher (found in ```efficientnet_classifier/efficientnet_poacher.py```) has predict methods for predicting on frames and on batches of frames. 

### Infer on a Video
```video_inference.py``` can be used to combine the image classifier and object detector to detect humans in a video obtained from the SPOTS drone. Currently, every frame is decomposed into 60 subframes. Then, each subframe is passed to the image classifier to determine if a human is present in the image. All images that were classified as 'human present' are then passed to the object detector for localisation.

<!-- ROADMAP -->
## Roadmap

- [x] Model training
- [x] Inference
    - [x] Inference on a single image
    - [x] Inference on a video
    - [x] Inference performance estimates (FPS)
- [x] Image preprocessing and augmentation

See the [open issues](https://github.com/github_username/repo_name/issues) for a full list of proposed features (and known issues).

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>


<!-- CONTACT -->
## Contact

Project Link: [AI-For-Wildlife-RGB](https://github.com/Samuel-Sendzul/AI-For-Wildlife-RGB)

<p align="right">(<a href="#top">back to top</a>)</p>
