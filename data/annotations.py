import xml.etree.ElementTree as ET
import json
import pandas as pd

def process_raw_annotations(annots_filepath):
    """Retrieve COCO style annotations from a JSON and process them into a more query friendly
    format (i.e. a Pandas dataframe).

    Args:
        annots_filepath (str): File path to annotations JSON.

    Returns:
        Pandas dataframe: The 'annotations' part of the COCO annotations JSON in dataframe format 
                          with the file_name of the image included.
    """
    # Retrieve annotations
    with open(annots_filepath, 'r') as fp:
        data = json.load(fp)

    # Prepare annotations
    images = pd.DataFrame(data['images'])
    annotations = pd.DataFrame(data['annotations'])  \
                    .drop(['id'], axis=1) \
                    .rename({
                        'image_id':'id'
                    }, axis=1)
    
    processed_annots = images[['file_name', 'id']].merge(annotations, on=['id'], how='inner')[['area', 'iscrowd', 'bbox', 'category_id', 'id', 'file_name']]

    return processed_annots.reset_index(drop=True)

def extract_targets(annotations, image_filename):
    """Extract the necessary annotation targets for an individual.

    Args:
        annotations (Pandas Dataframe): Annotations as returned by process_raw_annotations.
        image_filename (str): Filename of image to extract targets (name must be the same as name in the COCO annotations JSON).

    Returns:
        dict: Dict containing the target elements for an individual image.
    """

    img_annots = annotations[annotations.file_name == image_filename]

    boxes = list(img_annots['bbox'].values)
    areas = list(img_annots['area'].values)
    labels = list(img_annots['category_id'].values)
    iscrowd = list(img_annots['iscrowd'].values)

    target = {}
    target["boxes"] = boxes
    target["labels"] = labels
    target["area"] = areas
    target["iscrowd"] = iscrowd
    target["image_id"] = img_annots['id'].values[0]
    
    return target


def retrieve_coco_annotations(annotations, image_filename):
    """Retrieve COCO annotations for a single image from a JSON file and convert them to PASCAL VOC
    for use by a PyTorch Dataset.

    Args:
        annotations (dict): Annotation dictionary as prepared by process_raw_annotations().
        image_filename (str): Name of image file (including extension).

    Returns:
        tuple: Tuple of targets required by the PyTorch DataLoader.
    """
    boxes = []
    areas = []
    labels = []
    iscrowd = []

    for annot in annotations:
        if annot['file_name'] == image_filename:
            # Get bbox
            xmin = int(annot['bbox'][0])
            ymin = int(annot['bbox'][1])
            xmax = int(annot['bbox'][0]) + int(annot['bbox'][2])
            ymax = int(annot['bbox'][1]) + int(annot['bbox'][3])
            boxes.append([xmin, ymin, xmax, ymax])

            # Get area
            areas.append(annot['area'])

            # Get label
            labels.append(annot['category_id'])

            # Get icrowd
            iscrowd.append(annot['iscrowd'])
    
    return boxes, areas, labels, iscrowd

def read_annots_xml(file_path, scale_perc=0.6):
    tree = ET.parse(file_path)
    root = tree.getroot()

    boxes = []
    
    for object in root.findall('object'):

        # Get bounding boxes
        for bbox in object.findall('bndbox'):
            xmin = int(int(bbox.find('xmin').text)*scale_perc)
            xmax = int(int(bbox.find('xmax').text)*scale_perc)
            ymin = int(int(bbox.find('ymin').text)*scale_perc)
            ymax = int(int(bbox.find('ymax').text)*scale_perc)

            boxes.append([xmin, ymin, xmax, ymax])
    
    return boxes
                

def read_annots_heridal(file_path, scale_perc=0.6):
    tree = ET.parse(file_path)
    root = tree.getroot()

    boxes = []
    

    for object in root.findall('object'):

        # Get bounding boxes
        for bbox in object.findall('bndbox'):
            xmin = int(int(bbox.find('xmin').text)*scale_perc)
            xmax = int(int(bbox.find('xmax').text)*scale_perc)
            ymin = int(int(bbox.find('ymin').text)*scale_perc)
            ymax = int(int(bbox.find('ymax').text)*scale_perc)

            boxes.append([xmin, ymin, xmax, ymax])
    
    return boxes
                

def read_annots_penn_fudan(file_path):
    with open(file_path) as fp:
        text = fp.readlines()

    # Get all bounding boxes
    boxes = []
    for line in text:
        if "Bounding box for" in line:
            box = line.split(":")[1].split()
            xmin = int(box[0].split("(")[1].split(",")[0])
            ymin = int(box[1].split(")")[0])
            xmax = int(box[3].split("(")[1].split(",")[0])
            ymax = int(box[4].split(")")[0])
        
            boxes.append([xmin, ymin, xmax, ymax])
    
    return boxes
