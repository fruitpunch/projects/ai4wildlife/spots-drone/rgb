import torch
import torchvision.transforms as transforms
import os
from .annotations import process_raw_annotations, extract_targets
from PIL import Image
import json
import pandas as pd
import numpy as np

pytorch_transforms = transforms.Compose([
            transforms.ToTensor()
])

class SPOTSDataset(torch.utils.data.Dataset):

    def __init__(self, ann_root, img_root):

        self.img_root = img_root

        with open(ann_root, 'r') as fp:
            self.annots = pd.DataFrame(json.load(fp))

        self.img_files = []
        for image in os.listdir(self.img_root):
            if image.endswith('.JPG'):
                self.img_files.append(image)

          
    def __getitem__(self, idx): 

        image_filename = self.img_files[idx].split(".JPG")[0]

        img_path = f"{self.img_root}/{image_filename}.JPG"
        img = np.asarray(Image.open(img_path).convert("RGB"))

        # Retrieve annotations and convert to torch tensors
        target = extract_targets(annotations=self.annots, image_filename=image_filename)
        target['labels'] = torch.as_tensor(target['labels'], dtype=torch.int64)
        target['area'] = torch.as_tensor(target['area'], dtype=torch.float32)
        target['iscrowd'] = torch.as_tensor(target['iscrowd'], dtype=torch.int64)

        # Convert bboxes to [xmin, ymin, xmax, ymax]
        boxes = []
        for box in target['boxes']:
            xmin = box[0]
            ymin = box[1]
            xmax = box[0] + box[2]
            ymax = box[1] + box[3]

            boxes.append([xmin, ymin, xmax, ymax])
        
        target['boxes'] = torch.as_tensor(boxes, dtype=torch.int64)

        return pytorch_transforms(img), target

    def __len__(self):
        counter = 0
        for image in os.listdir(self.img_root):
            if image.endswith('.JPG'):
                counter += 1
        
        return counter