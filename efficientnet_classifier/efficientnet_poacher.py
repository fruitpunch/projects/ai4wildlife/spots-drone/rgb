import tensorflow as tf
from tensorflow.keras import layers
from tensorflow.keras.applications import EfficientNetB0

import numpy as np

from .utils.data import create_test_data_loader, create_train_data_loader, compute_class_weights
from .utils.constants import MODELS_CACHE_DIR, TRAIN_DIR, TEST_DIR, TRAIN_H_DIR, TRAIN_NH_DIR

import json

def unfreeze_model(model):
    # We unfreeze the top 20 layers while leaving BatchNorm layers frozen
    for layer in model.layers[-20:]:
        if not isinstance(layer, layers.BatchNormalization):
            layer.trainable = True

    optimizer = tf.keras.optimizers.Adam(learning_rate=1e-4)
    model.compile(
        optimizer=optimizer, loss="binary_crossentropy", metrics=["accuracy", tf.keras.metrics.AUC(num_thresholds=300)]
    )

class EfficientNetPoacher:
    """Class that contains all relevant methods for instantiating, training, and obtaining
    predictions from a image classifier.
    """
    def __init__(self, model_name):
        self.model_name = model_name
        
        self.img_size = 224

    def preprocess_data(self, batch_size):
        """Method to create the dataloaders used for training and validation the model.
        """

        self.train_ds, self.val_ds = create_train_data_loader(
            target_size=self.img_size, 
            batch_size=batch_size, 
            train_path=TRAIN_DIR)


    def compile_model_architecture(self):
        """Method to compile the EfficientNetB0 model with a custom classification head.
        You can see more about the classifier here: https://www.tensorflow.org/api_docs/python/tf/keras/applications/efficientnet/EfficientNetB0

        The classifier has been pretrained and is merely finetuned on the provided dataset.
        """
        inputs = layers.Input(shape=(self.img_size, self.img_size, 3))
        model = EfficientNetB0(include_top=False, input_tensor=inputs, weights="imagenet")

        # Freeze the pretrained weights
        model.trainable = False

        # Rebuild top
        x = layers.GlobalAveragePooling2D(name="avg_pool")(model.output)
        x = layers.BatchNormalization()(x)

        top_dropout_rate = 0.2
        x = layers.Dropout(top_dropout_rate, name="top_dropout")(x)
        outputs = layers.Dense(1, activation="sigmoid", name="pred")(x)

        # Compile
        model = tf.keras.Model(inputs, outputs, name="EfficientNet")
        optimizer = tf.keras.optimizers.Adam(learning_rate=1e-2)
        model.compile(
            optimizer=optimizer, loss="binary_crossentropy", metrics=["accuracy", tf.keras.metrics.AUC(num_thresholds=300)]
        )
        
        self.model = model


    def train(self, num_epochs_head, num_epochs_finetune):
        """A method used to finetune the image classifier on a custom dataset.
        """
        # Compute class weights
        class_weights = compute_class_weights(h_dir=TRAIN_H_DIR, nh_dir=TRAIN_NH_DIR)

        # Train head
        _history = self.model.fit(
            self.train_ds,
            validation_data=self.val_ds,
            epochs=num_epochs_head,
            verbose=2,
            class_weight=class_weights)

        self.history_head = _history.history

        unfreeze_model(self.model)

        _history = self.model.fit(
            self.train_ds,
            validation_data=self.val_ds,
            epochs=num_epochs_finetune,
            verbose=2,
            class_weight=class_weights)

        self.history_unfreeze = _history.history

        self.model.save(MODELS_CACHE_DIR.joinpath(f"{self.model_name}"))

    def load_model(self, save_path):
        self.model = tf.keras.models.load_model(save_path)

    def evaluate_on_set(self, save_results_file, batch_size):
        """A method used to obtain performance metrics on a provided dataset. 
        The metrics are saved to a JSON file.

        Args:
            save_results_file (str): Path to file where predictions should be saved.
        """
        # Load in model
        model = tf.keras.models.load_model(MODELS_CACHE_DIR.joinpath(f"{self.model_name}"))

        # Load data
        self.test_ds = create_test_data_loader(
            target_size=self.img_size,
            batch_size=batch_size,
            test_path=TEST_DIR
        )

        # Evaluate model
        print("Evaluate on test data")
        results = model.evaluate(self.test_ds)
        print("test loss, test acc:, test_auc:", results)

        # Save results to file
        results_dict = {}
        results_dict['test_loss'] = results[0]
        results_dict['test_acc'] = results[1]
        results_dict['test_auc'] = results[2]

        with open(save_results_file, 'w') as fp:
            json.dump(results_dict, fp,  indent=4)

    def predict_on_frame(self, img):
        """"""
        
        # Resize image 
        img = tf.image.resize_with_pad(img, 224, 224)

        # Predict
        x = tf.keras.preprocessing.image.img_to_array(img)
        x = np.expand_dims(x, axis=0)
        raw_pred = 1 - self.model.predict(x).item()

        return raw_pred

    def predict_on_batch(self, images):

        # Resize and add batching dimension
        images = [np.expand_dims(tf.image.resize_with_pad(image, 224, 224), axis=0) for image in images]

        # Create batch
        batch = np.vstack(images)

        # Predict on batch
        preds = self.model.predict(batch)

        # Post process predictions
        preds = np.array([1 - pred.item() for pred in preds])

        return preds




