"""Evaluate an image classification model on a test set.
"""
from efficientnet_poacher import EfficientNetPoacher

if __name__ == "__main__":

    # Instantiate model class object
    m = EfficientNetPoacher(model_name="poacher-classifier")

    # Predict on the test set
    m.evaluate_on_set(f"{m.model_name}-results.json", 128)