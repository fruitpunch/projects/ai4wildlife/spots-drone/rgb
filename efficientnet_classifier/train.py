"""A script that can be used to finetune the Poacher classifier. 
Plots of the training and validation metric curves are produced and saved to files.
"""

from efficientnet_poacher import EfficientNetPoacher
from utils.plot import plot_training_metrics_per_epoch
import argparse

if __name__ == "__main__":

    # Parse args
    parser = argparse.ArgumentParser(description='Train an image classification model.')
    parser.add_argument('--batch_size',
                        type=int,
                        help='Batch size for training and validation.',
                        default=128)
    parser.add_argument('--num_epochs_head',
                    type=int,
                    help='Number of epochs for training the head of the model.',
                    default=20)
    parser.add_argument('--num_epochs_finetune',
                    type=int,
                    help='Number of epochs for finetuning the model.',
                    default=10)
    parser.add_argument('--model_name',
                    type=str,
                    help='Name of model for saving training checkpoints',
                    default='efficientnet-poacher-detector')

    args = parser.parse_args()

    # Instantiate model class object
    m = EfficientNetPoacher(model_name=args.model_name)
    m.preprocess_data(batch_size=args.batch_size)

    m.compile_model_architecture()

    # Check the tensor shapes per layer
    m.model.summary()

    # Train and save object
    m.train(num_epochs_head=args.num_epochs_head, num_epochs_finetune=args.num_epochs_finetune)

    # Check training results
    plot_training_metrics_per_epoch(
        epochs=[i for i in range(args.num_epochs_head)],
        acc=m.history_head['accuracy'],
        val_acc=m.history_head['val_accuracy'],
        loss=m.history_head['loss'],
        val_loss=m.history_head['val_loss'],
        figname="Head")

    plot_training_metrics_per_epoch(
        epochs=[i for i in range(args.num_epochs_finetune)],
        acc=m.history_unfreeze['accuracy'],
        val_acc=m.history_unfreeze['val_accuracy'],
        loss=m.history_unfreeze['loss'],
        val_loss=m.history_unfreeze['val_loss'],
        figname="Unfreeze")

