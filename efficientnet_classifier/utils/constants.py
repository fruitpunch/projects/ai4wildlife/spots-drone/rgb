
import pathlib
import os

# Get project's root directory
dir_path = os.path.dirname(os.path.realpath(__file__))
root_dir = pathlib.Path(dir_path).parent

# Utilities directories
UTILS_DIR = root_dir.joinpath("utils")

# Model directories
MODELS_DIR = root_dir.joinpath("models")
MODELS_CACHE_DIR = MODELS_DIR.joinpath("cache")

# Data directories
DATA_DIR = root_dir.joinpath("SPOTS/imageClassification")
TRAIN_DIR = DATA_DIR.joinpath("train")
TEST_DIR = DATA_DIR.joinpath("test")
TRAIN_H_DIR = TRAIN_DIR.joinpath("human")
TRAIN_NH_DIR = TRAIN_DIR.joinpath("no_human")
TEST_H_DIR = TEST_DIR.joinpath("human")
TEST_NH_DIR = TEST_DIR.joinpath("no_human")