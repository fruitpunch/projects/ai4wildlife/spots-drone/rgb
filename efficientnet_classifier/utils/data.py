import tensorflow as tf
import os
import sklearn

def compute_class_weights(h_dir, nh_dir):
	num_nh = len(os.listdir(nh_dir))
	num_h = len(os.listdir(h_dir))

	y_true = [0]*num_nh + [1]*num_h

	class_weights = sklearn.utils.class_weight.compute_class_weight('balanced', classes=[0, 1], y=y_true)

	return {0:class_weights[0], 1:class_weights[1]}

def create_train_data_loader(target_size, batch_size, train_path): 
	train_ds = tf.keras.preprocessing.image_dataset_from_directory(
				train_path,
				validation_split=0.2,
				subset="training",
				seed=123,
				image_size=(target_size, target_size),
				batch_size=batch_size)

	val_ds = tf.keras.preprocessing.image_dataset_from_directory(
		train_path,
		validation_split=0.2,
		subset="validation",
		seed=123,
		image_size=(target_size, target_size),
		batch_size=batch_size)

	AUTOTUNE = tf.data.AUTOTUNE

	train_ds = train_ds.cache().prefetch(buffer_size=AUTOTUNE)
	val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)

	return train_ds, val_ds


def create_test_data_loader(target_size, batch_size, test_path):
	test_ds = tf.keras.preprocessing.image_dataset_from_directory(
		test_path,
		seed=123,
		image_size=(target_size, target_size),
		batch_size=batch_size)

	return test_ds