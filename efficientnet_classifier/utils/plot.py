
import os
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


def visualise_png_jpg_images(train_dir, fname):
	img_path = os.path.join(train_dir, fname)
	fig = plt.gcf()
	img = mpimg.imread(img_path)  # Get image in a file dir
	plt.imshow(img)
	plt.show()


def plot_training_metrics_per_epoch(epochs, acc, val_acc, loss, val_loss, figname):
	# Plot training and validation accuracy per epoch
	plt.figure()
	if len(acc) > 0:
		plt.plot(epochs, acc, label='acc')
	if len(val_acc) > 0:
		plt.plot(epochs, val_acc, label='val_acc')
	plt.title('Training and validation accuracy')
	plt.legend()
	plt.savefig(figname + ' Training and validation accuracy' + ".png")

	# Plot training and validation loss per epoch
	plt.figure()
	if len(loss) > 0:
		plt.plot(epochs, loss, label='loss')
	if len(val_loss) > 0:
		plt.plot(epochs, val_loss, label='val_loss')
	plt.title('Training and validation loss')
	plt.legend()
	plt.savefig(figname + ' Training and validation loss' +  ".png")
