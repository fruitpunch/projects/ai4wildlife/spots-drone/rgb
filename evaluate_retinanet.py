"""Obtain test set metrics for the RetinaNet model.
"""
from references.detection.engine import evaluate
from object_detector import ObjectDetector

from data.spots_dataset import SPOTSDataset

from references.detection.utils import collate_fn
import torch


if __name__ == "__main__":

    device = "cuda" if torch.cuda.is_available() else "cpu"

    object_detector = ObjectDetector("models/cache")    
    object_detector.compile_retinanet()
    object_detector.load_model_from_checkpoint(checkpoint_path="models/cache/retinanet-ckpt-5.pth")

    test_dataset = SPOTSDataset(img_root='SPOTS/objectDetection/test',
                                ann_root='SPOTS/objectDetection/test/test_annotations_coco.json')

    test_dataloader = torch.utils.data.DataLoader(
        test_dataset, batch_size=8, shuffle=True, collate_fn=collate_fn, num_workers=4)

    evaluate(object_detector.model, test_dataloader, device)