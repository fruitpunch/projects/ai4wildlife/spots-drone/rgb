"""After manually removing substandard images from a directory, this script will align the annotations JSON for the directory to only contain
annotations for the remaining images.
"""

import os
import json
import pandas as pd

if __name__ == "__main__":
    img_root = "/home/samuels/Documents/aiForWildlife/AI-For-Wildlife-RGB/spots/preprocessed"
    anno_root = "/home/samuels/Documents/aiForWildlife/AI-For-Wildlife-RGB/spots/preprocessed/subframe_annotations_coco.json"

    # Retrieve image files
    image_files = []
    for file in os.listdir(img_root):
        if file.endswith('.JPG'):
            image_files.append(file)

    images_keep = pd.DataFrame(image_files, columns=['file_name'])
    
    # Import annotations
    with open(anno_root, 'r') as fp:
        data = json.load(fp)

    # Prepare annotations
    images = pd.DataFrame(data['images'])
    annotations = pd.DataFrame(data['annotations'])


    # Keep only those annotations that match images in the directory
    images = images.merge(images_keep, on=['file_name'], how='inner')
    annotations = (
                    annotations.merge(images['id'], left_on='image_id', right_on='id', how='inner') 
                    .drop('id_y', axis=1)
                    .rename({
                        'id_x':'id'
                    }, axis=1)
                )           

    data['annotations'] = annotations.to_dict(orient='records')
    data['images'] = images.to_dict(orient='records')

    # Save file
    with open(anno_root, 'w') as outputfile:
            json.dump(data, outputfile)


