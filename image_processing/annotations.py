import json
import pandas as pd

def process_raw_annotations(annots_filepath):
    """Retrieve COCO style annotations from a JSON and process them into a more query friendly
    format (i.e. a Pandas dataframe).

    Args:
        annots_filepath (str): File path to annotations JSON.

    Returns:
        Pandas dataframe: The 'annotations' part of the COCO annotations JSON in dataframe format 
                          with the file_name of the image included.
    """
    # Retrieve annotations
    with open(annots_filepath, 'r') as fp:
        data = json.load(fp)

    # Prepare annotations
    images = pd.DataFrame(data['images'])
    annotations = pd.DataFrame(data['annotations'])  \
                    .drop(['id'], axis=1) \
                    .rename({
                        'image_id':'id'
                    }, axis=1)
    
    processed_annots = images[['file_name', 'id']].merge(annotations, on=['id'], how='inner')[['area', 'iscrowd', 'bbox', 'category_id', 'id', 'file_name']]

    return processed_annots

def extract_targets(annotations, image_filename):
    """Extract the necessary annotation targets for an individual image.

    Args:
        annotations (Pandas Dataframe): Annotations as returned by process_raw_annotations.
        image_filename (str): Filename of image to extract targets (name must be the same as name in the COCO annotations JSON).

    Returns:
        dict: Dict containing the target elements for an individual image.
    """

    img_annots = annotations[annotations.file_name == image_filename]

    boxes = list(img_annots['bbox'].values)
    areas = list(img_annots['area'].values)
    labels = list(img_annots['category_id'].values)
    iscrowd = list(img_annots['iscrowd'].values)

    target = {}
    target["boxes"] = boxes
    target["labels"] = labels
    target["area"] = areas
    target["iscrowd"] = iscrowd
    
    return target