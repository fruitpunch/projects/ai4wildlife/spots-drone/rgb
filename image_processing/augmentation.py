"""This script can be used to augment the SPOTS preprocessed video frames. 

{task} can be replaced by either imageClassification or objectDetection.
"""

import os
import albumentations as A
import cv2
from annotations import process_raw_annotations, extract_targets
import numpy as np
import json
import time
import datetime

BOX_COLOR = (255, 0, 0) # Red
TEXT_COLOR = (255, 255, 255) # White


def visualize_bbox(img, bbox, class_name, color=BOX_COLOR, thickness=2):
    """Visualizes a single bounding box on the image"""
    x_min, y_min, w, h = bbox
    x_min, x_max, y_min, y_max = int(x_min), int(x_min + w), int(y_min), int(y_min + h)
   
    cv2.rectangle(img, (x_min, y_min), (x_max, y_max), color=color, thickness=thickness)
    
    ((text_width, text_height), _) = cv2.getTextSize(class_name, cv2.FONT_HERSHEY_SIMPLEX, 0.35, 1)    
    cv2.rectangle(img, (x_min, y_min - int(1.3 * text_height)), (x_min + text_width, y_min), BOX_COLOR, -1)
    cv2.putText(
        img,
        text=class_name,
        org=(x_min, y_min - int(0.3 * text_height)),
        fontFace=cv2.FONT_HERSHEY_SIMPLEX,
        fontScale=0.35, 
        color=TEXT_COLOR, 
        lineType=cv2.LINE_AA,
    )
    return img


def visualize(image, bboxes, category_ids, category_id_to_name):
    img = image.copy()
    for bbox, category_id in zip(bboxes, category_ids):
        class_name = category_id_to_name[category_id]
        img = visualize_bbox(img, bbox, class_name)

    
    while cv2.waitKey(0) < 1:
        cv2.imshow('Frame', img)

def augment(img_root, annots_root, directory, num_rounds, output_folder, pr_rate):
    # Retrieve image files
    image_files = []
    for file in os.listdir(img_root):
        if file.endswith('.JPG'):
            image_files.append(file)

    # Retrieve annotations
    raw_annotations = process_raw_annotations(annots_root)
    

    # Create augmentation pipeline
    transform = A.Compose([
            A.RandomCrop(width=400, height=400),
            A.HorizontalFlip(p=0.5),
            A.RandomBrightnessContrast(p=0.2),
        ], bbox_params=A.BboxParams(format='coco', min_area=1000, label_fields=['labels']))

    # Output JSON filename
    file_name = output_folder + f'/{directory}_annotations_coco.json'

    # Augment images
    rnd = 1
    id_img = 0
    id_ann = 0
    images = []
    annotations = []
    # intial time
    t_i = time.time()
    for i in range(num_rounds):
        print(' ')
        print('-'*38)
        print(f'Round {rnd} started...')
        print('-'*38)
        for i, image_file in enumerate(image_files):

            id_img += 1

            if i == 0:
                print(' ')
                print('-'*38)
                print('Augmentation started...')
                print('-'*38)

            elif i == len(image_files) - 1:
                print('-'*38)
                print('Augmentation finished!')
                print('-'*38)

            # Extract targets for image
            target = extract_targets(annotations=raw_annotations, image_filename=image_file)

            # Read in image
            image = cv2.imread(f"{img_root}/{image_file}")

            # Augment
            transformed = transform(image=image, bboxes=target['boxes'], labels=target['labels'])

            # Save image file
            image_file_no_ext = image_file.split('.')[0]
            ext = image_file.split('.')[1]
            aug_image_file = f"{image_file_no_ext}_{rnd}"
            cv2.imwrite(f"{output_folder}/{aug_image_file}.{ext}", transformed['image'])

            # Save new annotations
            dico_img = {
                "license": 1,
                "file_name": aug_image_file,
                "coco_url": "None",
                "height": transformed['image'].shape[1],
                "width": transformed['image'].shape[2],
                "date_captured": "None",
                "flickr_url": "None",
                "id": id_img
            }

            images.append(dico_img)

            # Bounding boxes
            bndboxes = transformed['bboxes']
            for b in range(len(bndboxes)):

                id_ann += 1

                bndbox = bndboxes[b]
                
                # Convert 
                x_min = int(np.round(bndbox[0]))
                y_min = int(np.round(bndbox[1]))
                box_w = int(np.round(bndbox[2]))
                box_h = int(np.round(bndbox[3]))

                coco_box = [x_min,y_min,box_w,box_h]

                # Area
                area = box_w*box_h

                # Store the values into a dict
                dico_ann = {
                        "segmentation": [[]],
                        "area": area,
                        "iscrowd": 0,
                        "image_id": id_img,
                        "bbox": coco_box,
                        "category_id": 1,
                        "id": id_ann
                }

                annotations.append(dico_ann)

            if i % pr_rate == 0:
                print('Image [{:<4}/{:<4}] done.'.format(i, len(image_files)))
        rnd += 1
        

    # final time
    t_f = time.time()

    print('Elapsed time : {}'.format(str(datetime.timedelta(seconds=int(np.round(t_f-t_i))))))
    print('-'*38)
    print(' ')

    coco_dic = {
        'info': {},
        'licenses': [],
        'categories': [{
        "id": 1,
        "name": "human",
        "supercategory": "humans"
        }]
    }

    new_dic = {
        'info': coco_dic['info'],
        'licenses': coco_dic['licenses'],
        'images': images,
        'annotations': annotations,
        'categories': coco_dic['categories']
    }

    # Export json file
    with open(file_name, 'w') as outputfile:
        json.dump(new_dic, outputfile)


if __name__ == "__main__":

    task = 'imageClassification'

    directories = ['train', 'valid', 'test']

    for directory in directories:

        os.mkdir(f'SPOTS/{task}/{directory}/augmented')

        # Fill in with your paths
        img_root = f'SPOTS/{task}/{directory}/preprocessed'
        annots_root = f'SPOTS/{task}/{directory}/preprocessed/subframe_annotations_coco.json'
        output_folder = f'SPOTS/{task}/{directory}/augmented'

        augment(img_root=img_root, 
                annots_root=annots_root,
                directory=directory,
                num_rounds=2,
                output_folder=output_folder,
                pr_rate=50)

        print(f"{directory} complete!")