import json
import pandas as pd
import shutil
import os
import random

if __name__ == "__main__":

    directory = 'test'

    # Read in annotation file
    with open(f'SPOTS/imageClassification/{directory}/augmented/{directory}_annotations_coco.json', 'r') as fp:
        data = json.load(fp)

    # Get the list of images with annotations
    images = pd.DataFrame(data['images'])[['file_name', 'id']]
    annotations = (
        pd.DataFrame(data['annotations'])[['image_id']]
        .rename(
            {'image_id':'id'},
            axis=1
        )
    )
    object_images = images.merge(annotations, on='id', how='inner')[['file_name']].drop_duplicates().reset_index(drop=True).values
    object_images = [str(file_name[0]) for file_name in object_images]

    # Place all images with annotations in the correct folder (SPOTS/imageClassifier/{directory}/humans)
    for image in object_images:
        print(image)
        shutil.move(f"SPOTS/imageClassification/{directory}/augmented/{image}.JPG", 
        f"SPOTS/imageClassification/{directory}/humans/{image}.JPG")

    # Get remaining images in the augmented folder and take a random sample (same size as the number of object inclusive images)
    no_object_images = os.listdir(f"SPOTS/imageClassification/{directory}/augmented")
    no_object_images_sample = random.sample(no_object_images, 423) # @TODO: Replace sample size with dynamically calculated size of humans directory 

    # Place all images with annotations in the correct folder (SPOTS/imageClassifier/humans)
    for image in no_object_images_sample:
        shutil.move(f"SPOTS/imageClassification/{directory}/augmented/{image}", 
        f"SPOTS/imageClassification/{directory}/no_humans/{image}")
