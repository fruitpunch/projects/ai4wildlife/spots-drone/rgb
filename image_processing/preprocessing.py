"""This script can be used to preprocess the SPOTS raw video frames.

{task} can be replaced by either imageClassification or objectDetection. 
"""

from sub_frames import Subframes
from PIL import Image 
import pandas as pd
import numpy as np
import time
import json
import os
import datetime
from annotations import process_raw_annotations, extract_targets

def sub_export(img_root, ann_root, width, height, output_folder, 
            overlap=False, strict=False ,pr_rate=50, 
            object_only=True, export_ann=True):
    '''
    Function that exports sub-frames created from a directory of images, and their associated new 
    annotations.

    This function uses the 'SubFrames' class for image processing.

    Parameters
    -----------
    img_root : str
        Path to images.

    ann_root : str
        Path to a coco-style dict (.json) containing annotations of 
        the initial dataset.

    width : int
        Width of the sub-frames.
    
    height : int
        Height of the sub-frames.
    
    output_folder : str
        Output folder path where to save sub-frames and new annotations.
    
    overlap : bool, optional
        Set to True to get an overlap of 50% between 
        2 sub-frames (default: False)
    
    strict : bool, optional
        Set to True get sub-frames of exact same size 
        (e.g width x height) (default: False)

    pr_rate : int, optional
        Console print rate of image processing progress.
        Default : 50
    
    object_only : bool, optional
        A flag used to choose between :
            - saving all the sub-frames of the entire image
            (set to False)
            - saving only sub-frames with objects
            (set to True, default)

    export_ann : bool, optional
        A flag used to choose between :
            - not exporting annotations with sub-frames
            (set to False)
            - exporting annotations with sub-frames
            (set to True, default
   
    Returns
    --------
    list

    a coco-type JSON file named 'coco_subframes.json'
    is created inside the subframes' folder
    
    '''

    # Get annotations
    annotations = process_raw_annotations(ann_root)

    image_files = []
    for file in os.listdir(img_root):
        if file.endswith('.jpg'):
            image_files.append(file)


    # Header
    all_results = [['filename','boxes','labels','HxW']]

    # intial time
    t_i = time.time()

    for i, image_file in enumerate(image_files):

        if i == 0:
            print(' ')
            print('-'*38)
            print('Sub-frames creation started...')
            print('-'*38)

        elif i == len(image_files) - 1:
            print('-'*38)
            print('Sub-frames creation finished!')
            print('-'*38)

        # Extract targets for image
        target = extract_targets(annotations=annotations, image_filename=image_file)

        # Read in image
        image = Image.open(img_root + '/' + image_file)

        # Get subframes
        sub_frames = Subframes(image_file, image, target, width, height, strict=strict)
        results = sub_frames.getlist(overlap=overlap)

        # Save
        sub_frames.save(results, output_path=output_folder, object_only=object_only)
        
        if object_only is True:
            for b in range(len(results)):
                if results[b][1]:
                    h = np.shape(results[b][0])[0] # np array
                    w = np.shape(results[b][0])[1] # np array
                    all_results.append([results[b][3],results[b][1],results[b][2],[h,w]])

        elif object_only is not True:
            for b in range(len(results)):
                h = np.shape(results[b][0])[0]
                w = np.shape(results[b][0])[1]
                all_results.append([results[b][3],results[b][1],results[b][2],[h,w]])

        if i % pr_rate == 0:
            print('Image [{:<4}/{:<4}] done.'.format(i, len(image_files)))
        

    # final time
    t_f = time.time()

    print('Elapsed time : {}'.format(str(datetime.timedelta(seconds=int(np.round(t_f-t_i))))))
    print('-'*38)
    print(' ')

    return_var = np.array(all_results)[:,:3].tolist()

    # Export new annos
    if export_ann is True:
        file_name = output_folder + '/subframe_annotations_coco.json'

        # Initializations
        images = []
        annotations = []
        id_img = 0
        id_ann = 0

        for i in range(1,len(all_results)):
            
            id_img += 1

            h = all_results[i][3][0]
            w = all_results[i][3][1]

            dico_img = {
                "license": 1,
                "file_name": all_results[i][0],
                "coco_url": "None",
                "height": h,
                "width": w,
                "date_captured": "None",
                "flickr_url": "None",
                "id": id_img
            }

            images.append(dico_img)

            # Bounding boxes
            if all_results[i][1]:
                
                bndboxes = all_results[i][1]

                for b in range(len(bndboxes)):

                    id_ann += 1

                    bndbox = bndboxes[b]
                    
                    # Convert 
                    x_min = int(np.round(bndbox[0]))
                    y_min = int(np.round(bndbox[1]))
                    box_w = int(np.round(bndbox[2]))
                    box_h = int(np.round(bndbox[3]))

                    coco_box = [x_min,y_min,box_w,box_h]

                    # Area
                    area = box_w*box_h

                    # Store the values into a dict
                    dico_ann = {
                            "segmentation": [[]],
                            "area": area,
                            "iscrowd": 0,
                            "image_id": id_img,
                            "bbox": coco_box,
                            "category_id": 1,
                            "id": id_ann
                    }

                    annotations.append(dico_ann)
        
        coco_dic = {
          'info': {},
          'licenses': [],
          'categories': [{
            "id": 1,
            "name": "human",
            "supercategory": "humans"
            }]
        }

        new_dic = {
            'info': coco_dic['info'],
            'licenses': coco_dic['licenses'],
            'images': images,
            'annotations': annotations,
            'categories': coco_dic['categories']
        }

        # Export json file
        with open(file_name, 'w') as outputfile:
            json.dump(new_dic, outputfile)

    return return_var
    
if __name__ == "__main__":

    task = 'imageClassification'

    directories = ['train', 'valid', 'test']

    if task == 'objectDetection':
        object_only = True
    else:
        object_only = False

    for directory in directories:

        os.mkdir(f'SPOTS/{task}/{directory}/preprocessed')

        # Fill in with your paths
        images_folder = f'SPOTS/{task}/{directory}/raw'
        annos_path = 'SPOTS/images/raw/spots_annotations.json'
        output_folder = f'SPOTS/{task}/{directory}/preprocessed'

        # Export
        json_dic = sub_export(
                img_root = images_folder,
                ann_root = annos_path,
                width = 600,
                height = 400,
                output_folder = output_folder,
                strict = True,
                object_only = False
            )

    print(f'{directory} complete!')