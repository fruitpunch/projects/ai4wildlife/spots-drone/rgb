import os
import random

if __name__ == "__main__":
    task = 'imageClassification'

    img_root = "SPOTS/images/raw"
    anno_root = "SPOTS/images/raw/spots_annotations.json"
    output_folder = f"SPOTS"

    # Retrieve image files
    image_files = []
    for file in os.listdir(img_root):
        if file.endswith('.jpg'):
            image_files.append(file)

    # Get train (50%), validation (25%), test (25%) split 
    train_len = int(len(image_files)*0.5)
    valid_len = int(len(image_files)*0.25)

    # Randomly sample 50% of the raw images for the training set
    train = random.sample(image_files, train_len)

    # Get the remaining images
    remaining_images = list(set(image_files).difference(set(train)))

    # Randomly sample 25% of the raw images for the validation set
    valid = random.sample(remaining_images, valid_len)

    # Keep the remaining images for the test set
    test = list(set(remaining_images).difference(set(valid)))

    folders = ['train', 'valid', 'test']

    # Create folders
    os.mkdir(f"{output_folder}/{task}")
    for sub_dir in folders:
        os.mkdir(f"{output_folder}/{task}/{sub_dir}")
        os.mkdir(f"{output_folder}/{task}/{sub_dir}/raw")
    
    # Perform the copying
    for i, ds in enumerate([train, valid, test]):
        for image in ds:
            os.popen(f'cp {img_root}/{image} {output_folder}/{task}/{folders[i]}/raw/{image}')

        print(f'{folders[i]} complete!') 
