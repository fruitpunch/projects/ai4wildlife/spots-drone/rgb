from preprocessing import process_raw_annotations, extract_targets
from sub_frames import Subframes
from PIL import Image

if __name__ == "__main__":
    image_filename = '3_humans_70m_2_frame30-1.001s.jpg'

    # insert path to annotation folder
    annotations = process_raw_annotations(
        '/home/samuels/Documents/aiForWildlife/AI-For-Wildlife-RGB/SPOTS/images/raw/spots_annotations.json')
    target = extract_targets(annotations, image_filename)

    # insert path to raw images folder
    subs = Subframes(img_name=image_filename,
                     image=Image.open(f'/home/samuels/Documents/aiForWildlife/AI-For-Wildlife-RGB/SPOTS/images/raw/{image_filename}'),
                     target=target, width=400, height=400, strict=True)
    results = subs.getlist()
    points_results = subs.topoints(results)

    # Display all subframes
    subs.visualise(results)

    # Display the bounded objects in subframes
    # subs.displayobjects(results, points_results)