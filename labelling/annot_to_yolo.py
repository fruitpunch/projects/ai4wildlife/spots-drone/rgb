import os
import pandas as pd

annotations_location = "../SPOTS/fixed_spots_labels"
output_location = "../SPOTS/"
image_width = 3840
image_height = 2160
category = "human"

def main():

    annot_list = []
    
    # Loop through annotation text files
    for label_file in sorted(os.listdir(annotations_location)):
        label_str = os.fsdecode(label_file)

        if label_str.endswith(".txt"):
            img_str = ".".join(label_str.split(".")[:2]) + ".jpg"

            with open(os.path.join(annotations_location, label_str)) as f:
                lines = f.readlines()

                # Loop through boxes in file
                for line in lines:
                    line = line.split(" ")

                    # Transform bounding box coordinates to yolo
                    box_width = float(line[3]) * image_width
                    box_height = float(line[4]) * image_height
                    box_left = float(line[1]) * image_width - box_width / 2
                    box_top = float(line[2]) * image_height - box_height / 2

                    annot_list.append(
                        [
                            img_str,
                            category,
                            box_left,
                            box_top,
                            box_height,
                            box_width,
                            image_width,
                            image_height,
                        ])

    df_bbox = pd.DataFrame(
        annot_list,
        columns=[
            "img_file",
            "category",
            "box_left",
            "box_top",
            "box_height",
            "box_width",
            "img_width",
            "img_height",
        ])

    df_bbox.to_csv(os.path.join(output_location, "final_annot.csv"))





if __name__ == "__main__":
    main()
