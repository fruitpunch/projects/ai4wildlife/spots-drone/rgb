# create a folder to store extracted images
import os
import cv2
import pandas as pd
import sys

def get_video_features(video_directory: str):
    """Print FPS and total frame count for the videos in a given directory.

    Args:
        video_directory (str): Location of the videos
    """

    directory = os.fsencode(video_directory)

    for video in os.listdir(directory):
        video_str = os.fsdecode(video)

        if video_str.endswith(".MP4"):

            vidcap = cv2.VideoCapture(os.path.join(video_directory, video_str))
            fps = vidcap.get(cv2.CAP_PROP_FPS)
            nr_frames = vidcap.get(cv2.CAP_PROP_FRAME_COUNT)
            print("Video:", video_str, "- FPS:", fps, "- Frame count:", nr_frames)


def extract_from_directory(video_directory: str, target_frame_directory: str, subsample_interval: int):
    """Extract frames from the videos in a given directory.

    Args:
        video_directory (str): Location of the videos
        target_frame_directory (str): Target directory of the extracted frames
        subsample_interval (int): Interval of frames to be saved
    """

    directory = os.fsencode(video_directory)

    for video in os.listdir(directory):
        video_str = os.fsdecode(video)
        video_str_no_extension = os.path.splitext(video_str)[0]

        if video_str.endswith(".MP4"):
            print("Extracting frames from", video_str, "...")

            count = 0

            vidcap = cv2.VideoCapture(os.path.join(video_directory, video_str))

            while True:
                success, image = vidcap.read()
                if not success:
                    break

                if count % subsample_interval == 0:

                    timestamp = vidcap.get(cv2.CAP_PROP_POS_MSEC)/1000

                    if not os.path.exists(os.path.join(target_frame_directory, video_str_no_extension)):
                        os.mkdir(os.path.join(target_frame_directory, video_str_no_extension))

                    cv2.imwrite(os.path.join(target_frame_directory, video_str_no_extension, "frame{:05d}-{:.4}s.jpg".format(count, timestamp)), image)
                
                count += 1


if __name__ == "__main__":

    video_dir = "data/"
    image_dir = "images/"
    subsample_interval = 30

    # get_video_features(video_dir)

    extract_from_directory(video_dir, image_dir, subsample_interval)


# todo check file format
# todo image preprocessing (stabilizing, colour scheme etc.)