## Follow the steps here:
https://aws.amazon.com/blogs/machine-learning/streamlining-data-labeling-for-yolo-object-detection-in-amazon-sagemaker-ground-truth/

## Steps
1. create S3 bucket with subfolders bounding_box/images, bounding_box/ground_truth_annots, bounding_box/yolo_annot_files
2. Sync images using `aws s3 sync . s3://spots-ground-truth/bounding_box/images/ --exclude "*" --include "*.jpg"`
3. Change input.json
4. run prep_gt_job.py
5. Create job in Mechanical turk
6. Wait until completion
7. create annot.csv from turk job run `python parse_annot.py`
8. download annot.csv locally: `aws s3 cp s3://spots-ground-truth/bounding_box/ground_truth_annots/yolo-bbox/annot.csv`
9. create yolo annotations from annot.csv `python create_annot.py`
10. run `python visualize.py IMG.jpg` with `IMG.txt` (yolo annot format file) in the same directory