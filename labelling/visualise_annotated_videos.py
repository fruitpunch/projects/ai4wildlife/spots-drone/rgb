import cv2
import os
import keyboard

import albumentations as A

image_folder = os.path.join('../SPOTS/spots_images')
label_folder = os.path.join('../SPOTS/spots_labels')

if __name__ == "__main__":

    for img_name in sorted(os.listdir(image_folder)):
        img_str = os.fsdecode(img_name)
        if img_str.endswith(".jpg"):

            scale_perc = 0.3

            # Import an image
            img = cv2.imread(os.path.join(image_folder, img_str))
            width = int(img.shape[1] * scale_perc)
            height = int(img.shape[0] * scale_perc)
            dim = (width, height)

            # resize image
            resized = cv2.resize(img, dim, interpolation=cv2.INTER_AREA)

            boxes = []
            img_name = img_name[:-4]

            if not os.path.exists(os.path.join(label_folder, img_name + '.txt')):
                continue

            with open(os.path.join(label_folder, img_name + '.txt')) as f:
                lines = f.readlines()

            for line in lines:
                # remove newline character and split
                _, x, y, w, h = [float(number) for number in line.strip().split()]
                x_center, y_center, w_box, h_box = x * width, y * height, \
                                                w * width, h * height

                xmin = round(x_center - w_box / 2)
                ymin = round(y_center - h_box / 2)
                xmax = round(x_center + w_box / 2)
                ymax = round(y_center + h_box / 2)

                boxes.append([xmin, ymin, xmax, ymax])

            for box in boxes:
                cv2.rectangle(resized, (int(box[0]), int(box[1])),
                            (int(box[2]), int(box[3])), (255, 0, 0), 1)

            # Show with OpenCV
            cv2.imshow(img_name, resized)
            k = cv2.waitKey()
            if k == 0:
                print(img_name)
            cv2.destroyAllWindows()
