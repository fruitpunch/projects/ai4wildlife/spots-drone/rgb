import cv2
import matplotlib.pyplot as plt

from object_detector import ObjectDetector

import argparse

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Test an object detection model.')
    parser.add_argument('--model_name',
                    type=str,
                    help='Name of model for saving training checkpoints',
                    default='retinanet')
    parser.add_argument('--img_path',
                    type=str,
                    help='Path of image to test model',
                    default='SPOTS/objectDetection/test/3_humans_80m_frame1260-42_S16_3.JPG')
    args = parser.parse_args()

    object_detector = ObjectDetector("models/cache")    

    if args.model_name == "retinanet":
        object_detector.compile_retinanet()
        object_detector.load_model_from_checkpoint(checkpoint_path="models/cache/retinanet-ckpt-5.pth")
    elif args.model_name == "fasterrcnn":
        object_detector.compile_fastrcnn()
        object_detector.load_model_from_checkpoint(checkpoint_path="models/cache/fastRCNN-ckpt-10.pth")

    img = cv2.imread(args.img_path)

    box_preds = object_detector.predict([img])
    # Draw the boxes on the original frame 
    for pred in box_preds:
        if pred != []:
            for box in pred['boxes']:
                cv2.rectangle(img, (int(box[0]), int(box[1])), (int(box[2]), int(box[3])), (255,0,0), 2)

    plt.imshow(img) 
    plt.show()