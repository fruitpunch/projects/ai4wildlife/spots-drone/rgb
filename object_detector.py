import numpy as np
import math

# RetinaNet
from torchvision.models.detection import retinanet_resnet50_fpn, \
    fasterrcnn_resnet50_fpn
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor

# Training
from references.detection.engine import train_one_epoch_retinanet, train_one_epoch_fastrcnn

from utils.nms import softnms

import torch
from torch.utils.tensorboard import SummaryWriter
import torchvision.transforms as transforms

import cv2
import matplotlib.pyplot as plt

import numpy as np
import math

pytorch_transforms = transforms.Compose([
    transforms.ToTensor()
])


def evaluate(model, data_loader, device, writer, epoch):
    """This function can be used to obtain validation metrics
    (bounding box regression loss and classification loss)
    for a given model on a given dataset.
    Args:
        model ([type]): [description]
        data_loader (torch.utils.data.DataLoader):
        The dataloader containing the data for validation.
        device (str): The device you are using (cpu or gpu).
        writer (torch.utils.tensorboard.SummaryWriter):
        Tensorboard writer object for a graphical representation of losses.
        epoch (int): Epoch number.
    """

    bbox_loss = []
    classification_loss = []
    for images, targets in data_loader:
        images = list(image.to(device) for image in images)
        targets = [{k: v.to(device) for k, v in t.items()} for t in targets]

        loss_dict = model(images, targets)

        bbox_loss.append(loss_dict['bbox_regression'].item())
        classification_loss.append(loss_dict['classification'].item())

    mean_bbox_loss = np.mean(bbox_loss)
    mean_class_loss = np.mean(classification_loss)

    print(f'Validation bbox_regression loss: {mean_bbox_loss}')
    print(f'Validation classification loss: {mean_class_loss}')

    writer.add_scalar('Bbox Regression Loss/valid', mean_bbox_loss, epoch)
    writer.add_scalar('Classification Loss/valid', mean_class_loss, epoch)

def evaluate_fastrcnn(model, data_loader, device, writer, epoch):
    """This function can be used to obtain validation metrics (bounding box regression loss and classification loss)
    for a given model on a given dataset.

    Args:
        model ([type]): [description]
        data_loader (torch.utils.data.DataLoader): The dataloader containing the data for validation.
        device (str): The device you are using (cpu or gpu).
        writer (torch.utils.tensorboard.SummaryWriter): Tensorboard writer object for a graphical representation of losses.
        epoch (int): Epoch number.
    """

    bbox_loss = []
    classification_loss = []
    for images, targets in data_loader:
        images = list(image.to(device) for image in images)
        targets = [{k: v.to(device) for k, v in t.items()} for t in targets]

        loss_dict = model(images, targets)

        bbox_loss.append(loss_dict['loss_box_reg'].item())
        classification_loss.append(loss_dict['loss_classifier'].item())
    
    mean_bbox_loss = np.mean(bbox_loss)
    mean_class_loss = np.mean(classification_loss)
    
    print(f'Validation loss_box_reg loss: {mean_bbox_loss}')
    print(f'Validation classification loss: {mean_class_loss}')

    writer.add_scalar('Bbox Regression Loss/valid', mean_bbox_loss, epoch)
    writer.add_scalar('Classification Loss/valid', mean_class_loss, epoch)

class ObjectDetector:
    def __init__(self, cache_path) -> None:
        # Save cache path for saving and loading pickled models
        self.cache_path = cache_path

        # Set device
        self.device = "cuda" if torch.cuda.is_available() else "cpu"

    def compile_retinanet(self):
        """A method to instantiate a RetinaNet model with
         a ResNet50 feature extraction backbone.
        """
        # use pretrained on COCO
        self.model = retinanet_resnet50_fpn(pretrained=True)

        # num_classes which is user-defined
        num_classes = 2  # 1 class (person) + background

        # replace classification layer params
        out_features = self.model.head.classification_head.conv[0].out_channels
        num_anchors = self.model.head.classification_head.num_anchors
        self.model.head.classification_head.num_classes = num_classes

        # update classification layer
        cls_logits = torch.nn.Conv2d(out_features, num_anchors * num_classes,
                                     kernel_size=3, stride=1, padding=1)
        torch.nn.init.normal_(cls_logits.weight, std=0.01)  # pytorch docs
        torch.nn.init.constant_(cls_logits.bias,
                                -math.log((1 - 0.01) / 0.01))  # pytorch docs

        # assign cls head to model
        self.model.head.classification_head.cls_logits = cls_logits

        # construct an optimizer
        self.params = [p for p in self.model.parameters() if p.requires_grad]

        # SGD
        self.optimizer = torch.optim.SGD(self.params, lr=0.0003,
                                         momentum=0.9, weight_decay=0.0005)

        # Put model on correct device
        self.model.to(self.device)

    def compile_fasterrcnn(self):
        # load a model pre-trained on COCO
        self.model = fasterrcnn_resnet50_fpn(pretrained=True)

        # replace the classifier with a new one, that has
        # num_classes which is user-defined
        num_classes = 2  # 1 class (person) + background

        # get number of input features for the classifier
        in_features = self.model.roi_heads.box_predictor.cls_score.in_features

        # replace the pre-trained head with a new one
        self.model.roi_heads.box_predictor = FastRCNNPredictor(in_features,
                                                               num_classes)

        # construct an optimizer
        self.params = [p for p in self.model.parameters() if p.requires_grad]

        # SGD
        self.optimizer = torch.optim.SGD(self.params, lr=0.005,
                                         momentum=0.9, weight_decay=0.0005)

        # Put model on correct device
        self.model.to(self.device)

    def save_checkpoint_after_epoch(self, model_name, epoch):
        """A method to save the model state after training for a given epoch.
        Args:
            model_name (str): Name of the model for saving.
            epoch (int): Epoch number.
        """
        torch.save({
            'epoch': epoch,
            'model_state_dict': self.model.state_dict(),
            'optimizer_state_dict': self.optimizer.state_dict(),
        }, self.cache_path.joinpath(f"{model_name}-ckpt-{epoch + 1}.pth"))

    def load_model_from_checkpoint(self, checkpoint_path, train=False):
        """A method for loading a model into the Python environment from a checkpoint.
        Args:
            checkpoint_path (str): Filepath to the checkpoint.
            train (bool, optional): Boolean flag indicating if you want
            the model set to training or eval mode. Defaults to False (eval mode).
        """
        checkpoint = torch.load(checkpoint_path, map_location=self.device)
        self.model.load_state_dict(checkpoint['model_state_dict'])
        self.optimizer.load_state_dict(checkpoint['optimizer_state_dict'])

        epoch = checkpoint['epoch']

        if train:
            # Put model in training mode
            self.model.train()
        else:
            # Put model in eval mode
            self.model.eval()

        print(f"Model loaded from checkpoint! Epoch: {epoch + 1}")
    
    def train_retinanet(self, num_epochs, train_dataloader, val_dataloader, model_name):
        """A method to train an object detection model.
        Args:
            num_epochs (int): Number of epochs to train for.
            train_dataloader (torch.utils.data.DataLoader): Training dataloader.
            val_dataloader (torch.utils.data.DataLoader): Validation dataloader.
            model_name (str): Name of model for saving.
        """
        # Create tensorboard writer
        writer = SummaryWriter()

        # Learning rate scheduler
        lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(
            self.optimizer, T_0=1, T_mult=2)

        for epoch in range(num_epochs):
            # Train for one epoch, printing every 10 iterations
            train_one_epoch_retinanet(self.model, self.optimizer, train_dataloader, self.device, epoch, print_freq=10, writer=writer, lr_scheduler=lr_scheduler)

            # Evaluate on the validation set dataset
            evaluate(self.model, val_dataloader, self.device, writer, epoch)

            # Save checkpooint
            self.save_checkpoint_after_epoch(model_name=model_name,
                                             epoch=epoch)

        writer.flush()
    
    def train_fastrcnn(self, num_epochs, train_dataloader, val_dataloader, model_name):
        """A method to train an object detection model.

        Args:
            num_epochs (int): Number of epochs to train for.
            train_dataloader (torch.utils.data.DataLoader): Training dataloader.
            val_dataloader (torch.utils.data.DataLoader): Validation dataloader.
            model_name (str): Name of model for saving.
        """
        # Create tensorboard writer
        writer = SummaryWriter()

        # Learning rate scheduler
        lr_scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(self.optimizer, T_0=1, T_mult=2)

        for epoch in range(num_epochs):

            # Train for one epoch, printing every 10 iterations
            train_one_epoch_fastrcnn(self.model, self.optimizer, train_dataloader, self.device, epoch, print_freq=10, writer=writer, lr_scheduler=lr_scheduler)

            # Evaluate on the validation set dataset
            evaluate_fastrcnn(self.model, val_dataloader, self.device, writer, epoch)

            # Save checkpooint
            self.save_checkpoint_after_epoch(model_name=model_name, epoch=epoch)

        writer.flush()

    def predict(self, images):
        """ A method to predict bounding box location from images
        Args:
            images (numpy array): Array of images for which prediction needs
             to be performed
        """
        # Convert to tensors
        img_tensors = [pytorch_transforms(image).to(self.device) for image in images]
        # Predict
        outputs = self.model(img_tensors)

        # Perform non-max suppression (filter from bounding box predictions
        # the ones with the lowest confidence scores
        supp_preds = [softnms(output, Nt=0.45, thresh=0.5, method='nms')
                      for output in outputs]
        
        return supp_preds
