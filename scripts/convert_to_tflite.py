"""This script can be used to convert a Keras model to a TensorflowLite model.
"""

import tensorflow as tf

model = tf.keras.models.load_model('/home/samuels/Documents/aiForWildlife/AI-For-Wildlife-RGB/effiecientNetClassifier/models/cache/efficientnet-poacher-detector')
concrete_func = model.signatures[
  tf.saved_model.DEFAULT_SERVING_SIGNATURE_DEF_KEY]
concrete_func.inputs[0].set_shape([1, 224, 224, 3]) # Set input shape to be static
converter = tf.lite.TFLiteConverter.from_concrete_functions([concrete_func])
# converter = tf.lite.TFLiteConverter.from_keras_model(model)
converter.optimizations = [tf.lite.Optimize.DEFAULT] # Use dynamic range quantization to 8-bit int 
tflite_model = converter.convert()

# Save the model.
with open('poacher_classifier.tflite', 'wb') as f:
  f.write(tflite_model)