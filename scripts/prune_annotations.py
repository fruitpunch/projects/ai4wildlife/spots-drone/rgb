"""After manual removal of images in a training set for object detection, remove the annotations corresponding to those
images that were manually removed.
"""

import json
import os
import pandas as pd

if __name__ == "__main__":
    task = "objectDetection"
    directory = 'valid'

    # Import annotations
    with open(f'../SPOTS/{task}/{directory}/augmented/{directory}_annotations_coco.json') as fp:
            data = json.load(fp)

    images_meta = data['images']

    # Get list of images
    image_files = []
    for file in os.listdir(f"../SPOTS/{task}/{directory}/augmented/"):
        if file.endswith('.JPG'):
            image_files.append(file)

    # Only keep the image metadata for the remaining images in the directory
    new_image_meta = []
    for annot in images_meta:
        if annot['file_name'] + ".JPG" in image_files:
            new_image_meta.append(annot)

    # Keep only the annotations corresponding to the image ids in new_image_meta
    new_image_meta_df = pd.DataFrame(new_image_meta)

    annotations_df = pd.DataFrame(data['annotations'])[['image_id', 'category_id', 'bbox', 'area', 'iscrowd']]
    annotations_df.rename({
        'image_id':'id'
    }, inplace=True, axis=1)

    new_annotations_df = annotations_df.merge(new_image_meta_df, on='id', how='inner')[['id', 'category_id', 'bbox', 'area', 'iscrowd', 'file_name']]

    # Write annotations to json
    new_annotations_df.to_json(f'../SPOTS/{task}/{directory}/augmented/{directory}_annotations_coco.json', orient='records')