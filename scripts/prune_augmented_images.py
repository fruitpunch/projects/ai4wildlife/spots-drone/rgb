import json
import pandas as pd
import os

if __name__ == "__main__":
    task = "objectDetection"
    directory = 'valid'

    with open(f'../SPOTS/{task}/{directory}/augmented/{directory}_annotations_coco.json') as fp:
            data = json.load(fp)

    annotations = pd.DataFrame(data['annotations'])

    # Group annotations by image_id and count
    annotations_grouped = annotations.groupby(by=['image_id']).count().reset_index()[['image_id', 'bbox']]
    annotations_grouped.columns = ['id', 'bbox']

    # Grab the filenames corresponding to th non-zero bbox images
    images = pd.DataFrame(data['images'])
    non_zero_ims = images.merge(annotations_grouped, on=['id'], how='inner')['file_name'].values
    # Remove those images not in the non_zero_ims list
    for image in list(images['file_name']):
            if image not in list(non_zero_ims):
                os.remove(f"../SPOTS/{task}/{directory}/augmented/{image}.JPG")
    