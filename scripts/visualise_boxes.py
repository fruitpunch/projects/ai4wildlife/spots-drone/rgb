
"""Visualise bounding box annotations on raw images.
"""

import cv2

from data.annotations import process_raw_annotations, extract_targets


if __name__ == "__main__":

    img_name = "1_human_70m_frame600-20_S17_3"
    directory = 'train'

    # Import an image
    img = cv2.imread(f'/home/samuels/Documents/aiForWildlife/AI-For-Wildlife-RGB/spots/split/{directory}/{img_name}.JPG')
    
    annotations = process_raw_annotations('/home/samuels/Documents/aiForWildlife/AI-For-Wildlife-RGB/spots/split/split_annotations_coco.json')

    target = extract_targets(annotations, img_name)

    boxes = []
    for box in target['boxes']:
        xmin = box[0]
        ymin = box[1]
        xmax = box[0] + box[2]
        ymax = box[1] + box[3]

        boxes.append([xmin, ymin, xmax, ymax])

    for box in boxes:
        cv2.rectangle(img, (int(box[0]), int(box[1])), (int(box[2]), int(box[3])), (255,0,0), 2) ## Annotations are [xmin, ymin, xmax, ymax]

    # Show with OpenCV
    while cv2.waitKey(0) < 1:
        cv2.imshow("Example", img)


