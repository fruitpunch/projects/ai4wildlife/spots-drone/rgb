"""Train an Object Detection model using PyTorch.
"""
from object_detector import ObjectDetector

from references.detection.utils import collate_fn

import torch

from data.spots_dataset import SPOTSDataset

from utils.constants import MODELS_CACHE_DIR
import argparse

if __name__ == "__main__":
    # Parse args
    parser = argparse.ArgumentParser(description='Train an object detection model.')
    parser.add_argument('--batch_size',
                        type=int,
                        help='Batch size for training and validation.',
                        default=8)
    parser.add_argument('--num_epochs',
                    type=int,
                    help='Number of epochs for training.',
                    default=10)
    parser.add_argument('--model_name',
                    type=str,
                    help='Name of model for saving training checkpoints',
                    default='retinanet')

    args = parser.parse_args()

    # Create Datasets
    # Supply the filepaths to your SPOTS dataset
    train_dataset = SPOTSDataset(img_root='SPOTS/objectDetection/train',
                                 ann_root='SPOTS/objectDetection/train/train_annotations_coco.json')
    val_dataset = SPOTSDataset(img_root='SPOTS/objectDetection/valid',
                               ann_root='SPOTS/objectDetection/valid/valid_annotations_coco.json')

    # Define training and validation data loaders
    train_dataloader = torch.utils.data.DataLoader(
        train_dataset, batch_size=args.batch_size, shuffle=True,
        collate_fn=collate_fn, num_workers=4)

    val_dataloader = torch.utils.data.DataLoader(
        val_dataset, batch_size=args.batch_size, shuffle=False,
        collate_fn=collate_fn, num_workers=4)

    m = ObjectDetector(cache_path=MODELS_CACHE_DIR)

    if args.model_name == 'fastRCNN':
        m.compile_fastrcnn()  

        # Train the model
        m.train_fastrcnn(num_epochs=args.num_epochs, train_dataloader=train_dataloader, val_dataloader=val_dataloader, model_name=args.model_name)

    else:
        m.compile_retinanet() # Depending on the model you are looking to finetune, change this method to the appropriate one

        # Train the model
        m.train_retinanet(num_epochs=args.num_epochs, train_dataloader=train_dataloader, val_dataloader=val_dataloader, model_name=args.model_name)

    
