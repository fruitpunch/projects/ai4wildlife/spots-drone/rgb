from google.cloud import storage

# Create bucket object
def create_bucket(bucket_name):
    storage_client = storage.Client("brave-arcadia-328316")
    bucket = storage_client.bucket(bucket_name)

    return bucket
