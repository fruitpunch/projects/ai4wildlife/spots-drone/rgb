import cv2
import json


class ConvertCOCOToYOLO:
    """
    Takes in the path to COCO annotations and outputs YOLO annotations in multiple .txt files.
    COCO annotation are to be JSON formart as follows:
        "annotations":{
            "area":2304645,
            "id":1,
            "image_id":10,
            "category_id":4,
            "bbox":[
                0::704
                1:620
                2:1401
                3:1645
            ]
        }
    """

    def __init__(self, img_folder, json_path):
        self.img_folder = img_folder
        self.json_path = json_path

    def convert_labels(self, width, height, xmin, ymin, xmax, ymax):
        """
        # https://github.com/ultralytics/yolov5/wiki/Train-Custom-Data
        One row per object
            Each row is class x_center y_center width height format.
            Box coordinates must be in normalized xywh format (from 0 - 1). If your boxes are in pixels, divide x_center and width by image width, and y_center and height by image height.
            Class numbers are zero-indexed (start from 0).
        """
        #

        dw = 1. / width
        dh = 1. / height
        x = (xmin + xmax) / 2.0
        y = (ymin + ymax) / 2.0
        w = xmax - xmin
        h = ymax - ymin
        x = x * dw
        w = w * dw
        y = y * dh
        h = h * dh
        return (x, y, w, h)

    def convert(self, annotation_key='annotations', img_id='id', cat_id='category_id',
                bbox='bbox'):
        # Enter directory to read JSON file
        data = json.load(open(self.json_path))

        check_set = set()
        # Retrieve data
        for img in data["images"]:
            print(img)

            image_id = img[img_id]
            file_name = img["file_name"]

            width = img["width"]
            height = img["height"]
            all_anns = list(
                filter(lambda ann: ann["image_id"] == image_id, data[annotation_key]))

            for ann in all_anns:
                print(ann["bbox"])
                category_id = ann[cat_id][0]
                x_min, y_min, x_max, y_max = ann[bbox]

                # Convert the data
                yolo_bbox = self.convert_labels(width, height, xmin=x_min, xmax=x_max,
                                                ymin=y_min, ymax=y_max)

                # Prepare for export

                filename = f'{file_name.replace(".JPG", "")}.txt'
                content = f"{category_id} {yolo_bbox[0]} {yolo_bbox[1]} {yolo_bbox[2]} {yolo_bbox[3]}"

                # Export
                if image_id in check_set:
                    # Append to existing file as there can be more than one label in each image
                    file = open(filename, "a")
                    file.write("\n")
                    file.write(content)
                    file.close()

                elif image_id not in check_set:
                    check_set.add(image_id)
                    # Write files
                    file = open(filename, "w")
                    file.write(content)
                    file.close()


# To run in as a class
if __name__ == '__main__':
    ConvertCOCOToYOLO(img_folder='heridal/output/frames/',
                      json_path='heridal/coco_subframes.json').convert()
