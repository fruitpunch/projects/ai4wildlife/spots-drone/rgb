import pathlib
import os

# Get project's root directory
dir_path = os.path.dirname(os.path.realpath(__file__))
root_dir = pathlib.Path(dir_path).parent

# Utilities directories
UTILS_DIR = root_dir.joinpath("utils")

# Model directories
MODELS_DIR = root_dir.joinpath("models")
MODELS_CACHE_DIR = MODELS_DIR.joinpath("cache")

# Data directories
CLASSIFIER_DATA = root_dir.joinpath('efficientNetClassifier/SPOTS')
TRAIN_DIR = CLASSIFIER_DATA.joinpath('train')
TEST_DIR = CLASSIFIER_DATA.joinpath('test')