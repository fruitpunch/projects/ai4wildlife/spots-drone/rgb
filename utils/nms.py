import numpy as np

def softnms(preds, Nt, thresh, method='linear', sigma=0.5):
    '''
    Function for applying the Non-Maximum Suppression 
    (NMS) filter and Soft-NMS.

    Parameters
    ----------
    preds : dict
        Contains, at least, 3 keys:
          - 'boxes' : list, containing a list of 
            predicted bounding boxes,
          - 'labels' : int, containing a list of labels
            associated to the bboxes,
          - 'scores' : float, containing confidence 
            scores associated to predictions.
    
    Nt : float
        IoU threshold to apply.

    thresh : float
        Scores threshold.
    
    method : str, optional
        Choose between:
          - 'nms' for classical non-soft NMS
          - 'linear' for linear Soft-NMS
          - 'gaussian' for gaussian Soft-NMS

        In this third case, it is possible to
        specify the variance, by changing 'sigma'.

        Default: 'linear'
    
    sigma : float, optional
        Variance of gaussian's curve.

        Default: 0.5


    Returns
    -------
    dict
        Contains the 3 initial keys including filtered
        values.

    Notes
    -----
    Based on:
      - https://github.com/DocF/Soft-NMS/blob/master/soft_nms.py
      - https://github.com/bharatsingh430/soft-nms/blob/master/lib/nms/cpu_nms.pyx

    '''

    boxes = np.array(preds['boxes'].cpu().detach().numpy())
    labels = np.array(preds['labels'].cpu().detach().numpy())
    scores = np.array(preds['scores'].cpu().detach().numpy())

    boxes_f = boxes.copy()
    labels_f = labels.copy()
    scores_f = scores.copy()

    if len(boxes)==0:
        return []

    if boxes.dtype.kind == "i":
		    boxes = boxes.astype("float")

    N = boxes.shape[0]
    ind = np.array([np.arange(N)])
    boxes = np.concatenate((boxes, ind.T), axis=1)

    x1 = boxes[:,0]
    y1 = boxes[:,1]
    x2 = boxes[:,2]
    y2 = boxes[:,3]

    areas = (x2 - x1 + 1) * (y2 - y1 + 1)

    for i in range(N):

        # temporary variables
        t_boxes = boxes[i, :].copy()
        t_score = scores[i].copy()
        t_area = areas[i].copy()
        pos = i + 1

        if i != N-1:
            max_score = np.max(scores[pos:], axis=0)
            max_pos = np.argmax(scores[pos:], axis=0)

        else:
            max_score = scores[-1]
            max_pos = 0

        if t_score < max_score:
            boxes[i, :] = boxes[max_pos + i + 1, :]
            boxes[max_pos + i + 1, :] = t_boxes
            t_boxes = boxes[i,:]

            scores[i] = scores[max_pos + i + 1]
            scores[max_pos + i + 1] = t_score
            t_score = scores[i]

            areas[i] = areas[max_pos + i + 1]
            areas[max_pos + i + 1] = t_area
            t_area = areas[i]

        # compute IoU
        xx1 = np.maximum(boxes[i, 0], boxes[pos:, 0])
        yy1 = np.maximum(boxes[i, 1], boxes[pos:, 1])
        xx2 = np.minimum(boxes[i, 2], boxes[pos:, 2])
        yy2 = np.minimum(boxes[i, 3], boxes[pos:, 3])


        w = np.maximum(0.0, xx2 - xx1 + 1)
        h = np.maximum(0.0, yy2 - yy1 + 1)

        # IoU
        iou = (w * h) / (areas[i] + areas[pos:] - (w * h))

        # Weigthing
        # ---
        # 1 - Linear
        if method == 'linear':
            weight = np.ones(iou.shape)
            weight[iou > Nt] = weight[iou > Nt] - iou[iou > Nt]
        # 2 - Gaussian
        elif method == 'gaussian':
            weight = np.exp(-(iou*iou)/sigma)
        # 3 - Original
        elif method == 'nms':
            weight = np.ones(iou.shape)
            weight[iou > Nt] = 0

        scores[pos:] = weight * scores[pos:]
  
    idx = boxes[:,4][scores > thresh]
    pick = idx.astype(int)

    return {'boxes':boxes_f[pick],'labels':labels_f[pick],'scores':scores_f[pick]}