"""Utility functions for processing raw SPOTS footage.
"""
import numpy as np
from typing import List
import albumentations as A


def get_sub_frames(image, width, height, strict=False, overlap=False) -> List:
    """ Function to split images into subfarmes
    Args:
        image: image object
        width (int): subframe width
        height (int): subframe height
        strict (bool): whether to allow subframes to cross image borders,
        default False
        overlap (bool): whether to create overlapping subframes, default False

    Returns:
        results (List): list of subframe images, length of number of subframes
    """
    # Image preprocessing
    img_width = image.size[0]
    img_height = image.size[1]

    results = []

    # Process
    image = np.array(image)

    # Crop lists
    if overlap is True:
        overlap = 0.5
        y_sub = int(np.round(height * overlap))
        x_sub = int(np.round(width * overlap))
        rg_ymax = img_height - y_sub
        rg_xmax = img_width - x_sub
    else:
        y_sub = height
        x_sub = width
        rg_ymax = img_height
        rg_xmax = img_width

    crops = []

    # iterate from 0 to max y coordinates with steps of subframe size
    for y in range(0, rg_ymax, y_sub):
        # if y coordinate + subframe height does not cross border of image
        if y + height <= img_height:
            for x in range(0, rg_xmax, x_sub):
                if x + width <= img_width:
                    xmin, ymin = x, y
                    xmax, ymax = x + width, y + height
                elif x + img_width % width <= img_width:
                    xmin, ymin = img_width - width, y
                    xmax, ymax = x + img_width % width, y + height

                if strict is True:
                    crops.append([xmin, ymin, xmax, ymax])
                else:
                    crops.append([x, y, xmax, ymax])

        # adjust coordinates if resulting y coordinates
        # would cross image border
        elif y + img_height % height <= img_height:
            for x in range(0, rg_xmax, x_sub):
                if x + width <= img_width:
                    xmin, ymin = x, img_height - height
                    xmax, ymax = x + width, y + img_height % height
                elif x + img_width % width <= img_width:
                    xmin, ymin = img_width - width, img_height - height
                    xmax, ymax = x + img_width % width, y + img_height % height

                if strict is True:
                    crops.append([xmin, ymin, xmax, ymax])
                else:
                    crops.append([x, y, xmax, ymax])

    sub = 0
    for xmin, ymin, xmax, ymax in crops:
        transf = A.Compose([A.Crop(xmin, ymin, xmax, ymax, p=1.0)])
        augmented = transf(image=image)
        results.append(augmented['image'])
        sub += 1

    return results


def translate_bbox(box: List, index) -> List:
    """
    Function to convert bbox coordinates in subframe to coordinates in full
    frame
    Args:
        box (List): bounding box coordinates in subframe
        index (int): the nth subframe

    Returns:
        List of bounding box coordinates in full frame
    """
    # Obtain coordinate of subframe in raw frame
    # todo why do we divide by ten
    row = index // 10
    col = index % 10

    # Translate bbox coordinates
    # 400 x 400 is size of the subframe
    xmin = box[0] + col * 400
    ymin = box[1] + row * 400
    xmax = box[2] + col * 400
    ymax = box[3] + row * 400

    return [xmin, ymin, xmax, ymax]
