"""
This script extracts annotations from csv and converts yolo to coco format
Output is stored in json file
"""

import os
import json
import pandas as pd
from datetime import date

if __name__ == "__main__":
    """
    This script extracts annotations from csv and converts yolo to coco format
    Output is stored in json file
    """
    annotations_location = 'SPOTS/images/annot.csv'
    output_location = 'SPOTS/images/raw/spots_annotations.json'
    yolo = pd.read_csv(os.path.join(os.getcwd(), annotations_location))


    # Initializations
    images = []
    annotations = []
    id_img = 1
    id_ann = 1

    # Capture the unique frames to notate bounding box(es) per image
    image_names = yolo['img_file'].unique()

    for image in image_names:
        id_img += 1
        dico_img = {
            "license": 1,
            "file_name": yolo[yolo['img_file'] == image]['img_file'].values[0],
            "coco_url": "None",
            "height": int(yolo[yolo['img_file'] == image]['img_height'].values[0]),
            "width": int(yolo[yolo['img_file'] == image]['img_width'].values[0]),
            "date_captured": "None",
            "flickr_url": "None",
            "id": id_img
        }
        images.append(dico_img)

        image_df = yolo[yolo['img_file'] == image]
        # Record bounding boxes
        for index, row in image_df.iterrows():
            id_ann += 1

            x_min = int(row['box_left'])
            y_min = int(row['box_top'])
            box_w = int(row['box_width'])
            box_h = int(row['box_height'])

            coco_box = [x_min, y_min, box_w, box_h]

            # Area
            area = box_w * box_h

            # Store the values into a dict
            dico_ann = {
                "segmentation": [[]],
                "area": area,
                "iscrowd": 0,
                "image_id": id_img,
                "bbox": coco_box,
                "category_id": 1,
                "id": id_ann
            }
            # note tab here to record all bboxes
            annotations.append(dico_ann)

    coco_dic = {
        'info': {},
        'licenses': [],
        'categories': [{
            "id": 1,
            "name": "human",
            "supercategory": "humans"
        }]
    }

    # Update info
    coco_dic['info']['date_created'] = str(date.today())
    coco_dic['info']['year'] = str(date.today().year)

    new_dic = {
        'info': coco_dic['info'],
        'licenses': coco_dic['licenses'],
        'images': images,
        'annotations': annotations,
        'categories': coco_dic['categories']
    }

    # Export json file
    with open(output_location, 'w') as outputfile:
        json.dump(new_dic, outputfile)

