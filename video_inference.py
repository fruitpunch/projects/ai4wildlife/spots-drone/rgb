"""Use the image classifier-object detector pipeline for video inference.
"""

import cv2
from utils.raw_footage import get_sub_frames

from utils.raw_footage import get_sub_frames, translate_bbox

from efficientnet_classifier.efficientnet_poacher import EfficientNetPoacher
from object_detector import ObjectDetector

import time

from PIL import Image
import cv2
import numpy as np


if __name__ == "__main__":

    input_video = 'SPOTS/videos/demo-rgb-raw.mp4'
    output_video = 'demo-rgb-annotated.avi'

    # Load in the models
    frame_classifier = EfficientNetPoacher("poacher_classifier")
    frame_classifier.load_model("models/cache/efficientnet-poacher-detector")

    object_detector = ObjectDetector("models/cache")    
    object_detector.compile_retinanet()
    object_detector.load_model_from_checkpoint(checkpoint_path="models/cache/retinanet-ckpt-5.pth")

    # Create a VideoCapture object and read from input file
    # If the input is the camera, pass 0 instead of the video file name
    cap = cv2.VideoCapture(input_video)

    # Create video writer
    out = cv2.VideoWriter(output_video, cv2.VideoWriter_fourcc('M','J','P','G'), 30, (int(cap.get(3)),int(cap.get(4))))

    # Check if camera opened successfully
    if (cap.isOpened()== False): 
        print("Error opening video stream or file")

    inference_times_object_detector = []
    inference_times_classifier = []

    # Read until video is completed
    while(cap.isOpened()):
        # Capture frame-by-frame
        ret, frame = cap.read()

        if ret == True:
            
            # Read in footage
            image_display = frame
            image = Image.fromarray(cv2.cvtColor(image_display, cv2.COLOR_BGR2RGB))

            # Get subframes
            # We do not want subframes outside of images borders,
            # therefore strict true
            sub_frames = get_sub_frames(image, width=400, height=400,
                                        strict=True)

            # Determine which subframes contain poachers
            t = time.time()
            sub_frame_preds = frame_classifier.predict_on_batch(sub_frames)
            inference_times_classifier.append(time.time() - t)

            # Get the indexes of the subframes containing poachers
            # We set 0.85 as the probability treshold
            poacher_idxs = list(np.where(sub_frame_preds > 0.85)[0] + 1)
            poacher_mask = sub_frame_preds > 0.85
            poacher_subframes = np.array(sub_frames)[poacher_mask]

            if len(poacher_subframes) > 0:

                # Pass the subframes to the object detector
                t = time.time()
                box_preds = object_detector.predict(poacher_subframes)
                inference_times_object_detector.append(time.time() - t)

                # Draw the boxes on the original frame 
                for pred, index in zip(box_preds, poacher_idxs):
                    if pred:
                        for box in pred['boxes']:
                            translated_box = translate_bbox(box, index)
                            cv2.rectangle(image_display,
                                          (int(translated_box[0]),
                                           int(translated_box[1])),
                                          (int(translated_box[2]),
                                           int(translated_box[3])),
                                          (255, 0, 0), 2)

                out.write(image_display)

        # Break the loop
        else: 
            break

    # When everything done, release the video capture object
    cap.release()
    out.release()
